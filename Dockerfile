FROM openjdk:17-oracle
RUN mkdir /tmp/spring3
ADD . /tmp/spring3
RUN chmod +x /tmp/spring3/gradlew
WORKDIR /tmp/spring3
RUN microdnf install findutils
RUN ./gradlew clean build
RUN ls /tmp/spring3/build/libs
RUN mv /tmp/spring3/build/libs/*.jar /tmp/app.jar

FROM openjdk:17-oracle
COPY --from=0 /tmp/app.jar /tmp
RUN ls /tmp
ENTRYPOINT ["java", "-jar", "/tmp/app.jar"]
EXPOSE 8080
