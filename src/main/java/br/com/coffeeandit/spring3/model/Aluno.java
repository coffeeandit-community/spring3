package br.com.coffeeandit.spring3.model;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class Aluno {

    private Integer id;

    @NotBlank
    private String nome;

    @NotNull
    private Curso curso;

    @Email
    private String email;

}
