package br.com.coffeeandit.spring3.api;

import jakarta.validation.ConstraintViolationException;
import jakarta.validation.constraints.Min;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController(value = AlunoController.BASE_PATH)
@Validated
public class AlunoController {

    public static final String BASE_PATH = "/coffeeandit/aulao";

    @GetMapping({"/ola", "/ola/"})
    public String ola() {
        return "Hello";
    }

    @GetMapping("/alunos/{id}")
    ResponseEntity<String> validaPathVariable(@PathVariable("id") @Min(5) int id) {
        return ResponseEntity.ok("valid");
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException e) {
        return new ResponseEntity<>("erro ao validar atributo: " + e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}