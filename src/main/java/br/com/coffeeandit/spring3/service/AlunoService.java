package br.com.coffeeandit.spring3.service;

import io.micrometer.observation.annotation.Observed;
import org.springframework.stereotype.Service;

@Observed(name = "alunoService")
@Service
public class AlunoService {

    public String ola() {
        return "ola";
    }

}
